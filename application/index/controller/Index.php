<?php
namespace app\index\controller;
use think\Controller;
class Index extends Base{

    public function index()
    {
        $base_api_url = config('base_api_url');
        $api_url = config('index_api');
    	$data = [
            'qq' => cookie('qq'),
        ];
        
        $request_url = $base_api_url.$api_url;
        // p($request_url);
        $res = https_request($request_url,$data);
		// p($res);
        if ($res['code'] == 0) {
            return $res['msg'];
        } 
        $this->assign('base_api_url',$base_api_url);
        $this->assign('user_data',$res['data']);
        return $this->fetch();
    }
}
