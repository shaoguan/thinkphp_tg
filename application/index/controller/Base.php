<?php
namespace app\index\controller;
use think\Controller;
class Base extends Controller{
	public function _initialize(){
		header("Content-type:text/html;charset=utf-8");
		$this->check_spider();
		$qq = trim(input('qq'));
	
		if (!isset($qq)) {
			if (cookie('?qq')) {
				$qq = cookie('qq');
			} else {
				$qq = config('my_qq');
			}
		}
		// 更新cookie中的uuid
		cookie('qq',$qq,60*60*24*7);
	}

	public function check_spider(){
        $spider = get_naps_bot();
        if($spider != false){
            // 有蜘蛛来访
            $tlc_thispage   = addslashes($_SERVER['HTTP_USER_AGENT']);
            $url            = $_SERVER['HTTP_REFERER'];
            $file           = "rob.txt"; //日志文件名 当前目录
            $time           = date("Y-m-d H:i:s");
            $data           = fopen($file,"a");
            fwrite($data,"Time:$time robot:$spider URL:$tlc_thispage referUrl:$url \n");
            fclose($data);
        }else{
            // 没有蜘蛛来访
        }
    }
}