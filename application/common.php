<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function p($array){
  dump($array,1,'<pre>',0);
}


function https_request($url, $data = null){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)){
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    // return $output; 
    return json_decode($output,true); // 转为数组
}

// 获取内容get_contents
function get_contents($url) {
    $ch = curl_init ();
    $timeout = 100;
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt ( $ch, CURLOPT_TIMEOUT, 2 );
    $file_contents = curl_exec ( $ch );
    curl_close ( $ch );
    return $file_contents;
}


function get_naps_bot(){  // 各类蜘蛛名称汇集，方便查看日志
        $useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (strpos($useragent,'googlebot')          !== false){return 'Googlebot';}     //谷歌网页
        if (strpos($useragent,'Googlebot-Image')    !== false){return 'Googlebot-Image';}   //谷歌图片
        if (strpos($useragent,'Googlebot-Mobile')   !== false){return 'Googlebot-Mobile';}  //谷歌无线
        if (strpos($useragent,'Mediapartners-Google')   !== false){return 'Mediapartners-Google';}  //谷歌AdSense
        if (strpos($useragent,'Adsbot-Google')          !== false){return 'Adsbot-Google';}  //谷歌AdWords
        
        if (strpos($useragent,'baiduspider')        !== false){return 'Baiduspider';}           //百度
        if (strpos($useragent,'BaiDuSpider-Image')  !== false){return 'BaiDuSpider-Image';}     //百度图片
        if (strpos($useragent,'BaiDuSpider-News')   !== false){return 'BaiDuSpider-News';}      //百度新闻
        if (strpos($useragent,'BaiDuSpider-Mobile') !== false){return 'BaiDuSpider-Mobile';}    //百度无线
        if (strpos($useragent,'BaiDuSpider-Video')  !== false){return 'BaiDuSpider-Video';}     //百度视频
        if (strpos($useragent,'BaiDuSpider-Cpro')   !== false){return 'BaiDuSpider-Cpro';}      //百度联盟
        
        if (strpos($useragent,'360Spider')      !== false){return '360Spider';}             //360
        if (strpos($useragent,'msnbot')         !== false){return 'MSNbot';}                //Bing
        if (strpos($useragent,'slurp')          !== false){return 'Yahoobot';}              // 雅虎
        if (strpos($useragent,'sosospider')     !== false){return 'Soso';}                  //搜搜
        if (strpos($useragent,'sohu-search')    !== false){return 'Sohubot';}               //搜狐
        if (strpos($useragent,'lycos')          !== false){return 'Lycos';}                 // Lycos
        if (strpos($useragent,'sogou spider')   !== false){return 'Sogou';}                 //搜狗
        if (strpos($useragent,'yodaobot')       !== false){return 'Yodao';}                 //有道1
        if (strpos($useragent,'OutfoxBot')      !== false){return 'Yodao2';}                //有道2
        if (strpos($useragent,'robozilla')      !== false){return 'Robozilla'; }            //robozilla
        if (strpos($useragent,'ia_archiver')    !== false){return 'alexa'; }                // alexa  
        if (strpos($useragent,'scooter ')       !== false){return 'altavista';}             // scooter
        if (strpos($useragent,'fast-webcrawler ')   !== false){ return 'alltheweb';}        //alltheweb蜘蛛
        if (strpos($useragent,'scooter ')           !== false){ return 'scooter';}   //altavista蜘蛛：scooter 
        return false;
    }